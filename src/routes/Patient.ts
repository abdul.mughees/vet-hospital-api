import express from 'express';
import patientController from '../controllers/patientController';
import { Schemas, ValidateSchema } from '../middleware/ValidateSchema';

const router = express.Router();

router.post('/create', ValidateSchema(Schemas.patients.create), patientController.createPatient);
router.get('/get/:patientId/unpaid', patientController.readAmountPayable)
router.get('/get/:patientId', patientController.readPatient);
router.get('/get/', patientController.readAll);
router.patch('/update/:patientId', ValidateSchema(Schemas.patients.update), patientController.updatePatient);
router.delete('/delete/:patientId', patientController.deletePatient);

export = router;