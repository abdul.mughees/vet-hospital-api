import express from 'express';
import appointmentController from '../controllers/appointmentController';
import { Schemas, ValidateSchema } from '../middleware/ValidateSchema';

const router = express.Router();

router.post('/create', ValidateSchema(Schemas.appointment.create), appointmentController.createAppointment);
router.get('/get/unpaid', appointmentController.readUnpaidAppointments);
router.get('/get/byDate', appointmentController.readAppointmentsByDate);
router.get('/get/patientAppointments', appointmentController.readAppointmentsByPatient);
router.get('/get/:appointmentId', appointmentController.readAppointment);
router.get('/get/', appointmentController.readAll);
router.patch('/update/:appointmentId', ValidateSchema(Schemas.appointment.update), appointmentController.updateAppointment);
router.delete('/delete/:appointmentId', appointmentController.deleteAppointment);

export = router;