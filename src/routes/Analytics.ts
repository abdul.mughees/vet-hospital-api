import express from 'express';
import analyticsController from '../controllers/analyticsController';

const router = express.Router();

router.get('/get/petReport', analyticsController.readPetReport);

export = router;