import Joi, { ObjectSchema } from 'joi';
import { Request, Response, NextFunction } from "express";
import { IPatient } from '../models/Patient';
import Appointment, { IAppointment } from '../models/Appointment';

export const ValidateSchema = (schema: ObjectSchema) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        try {
            await schema.validateAsync(req.body);

            next();
        } catch (error) {
            console.log("Error: ", error);
            return res.status(422).json({ error });
        }
    }
}

export const Schemas = {
    patients: {
        create: Joi.object<IPatient>({
            petName: Joi.string().required(),
            petType: Joi.string().required(),
            ownerName: Joi.string().required(),
            ownerAddress: Joi.string().required(),
            ownerPhone: Joi.string().required()
        }),
        update: Joi.object<IPatient>({
            petName: Joi.string(),
            petType: Joi.string(),
            ownerName: Joi.string(),
            ownerAddress: Joi.string(),
            ownerPhone: Joi.string()
        })
    },

    appointment: {
        create: Joi.object<IAppointment>({
            startTime: Joi.string().required(),
            endTime: Joi.string().required(),
            description: Joi.string().required(),
            paymentCurrency: Joi.string().required(),
            amount: Joi.string().required(),
            patient: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
        }),
        update: Joi.object<IAppointment>({
            startTime: Joi.string(),
            endTime: Joi.string(),
            description: Joi.string(),
            paymentCurrency: Joi.string(),
            amount: Joi.string(),
            patient: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
        })
    }
}