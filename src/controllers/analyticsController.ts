import { Request, Response, NextFunction } from "express";
import mongoose from "mongoose";
import Appointment from "../models/Appointment";
import Patient from "../models/Patient";

const readPetReport = (req: Request, res: Response, next: NextFunction) => {
    return Patient.aggregate(
        [
            { "$group": { "_id": "$petType", "count": { "$sum": 1 } } },
            { $sort: { count: -1 } },
            { $limit: 1 },
        ], function (err: any, results: any) {
            if (!err) {
                res.status(200).json({ message: results });
            } else {
                res.status(500).json({ message: err })
            }
        }
    )
}

const readWeeklyReport = (req: Request, res: Response, next: NextFunction) => {

}

export default { readPetReport }