jest.useFakeTimers()
import patientController from "./patientController";
import mongoose from "mongoose";

const id = new mongoose.Types.ObjectId();

describe("Create Patient Record", () => {
  it("Patient record is create", async () => {
    const req: any = {
      body: {
        _id: id,
        petName: "Shaka",
        petType: "Dog",
        ownerName: "Rafique",
        ownerAddress: "New Address",
        ownerPhone: "1234567890",
      },
    };

    const res: any = {
      status: function () {
        return this;
      },
      json: function () {},
    };

    patientController
      .createPatient(req, res, () => {})
      .then((savedPatient) => {
        expect(savedPatient).toContain(id);
      });
  });

  it("Patient record is Read", async () => {
    const req: any = {
      params: {
        patientId: id,
      },
    };

    const res: any = {
      status: function () {
        return this;
      },
      json: function () {},
    };

    patientController
      .readPatient(req, res, () => {})
      .then((savedPatient) => {
        expect(req.body);
      });
  });

  it("Payable Amount is read", async () => {
    const req: any = {
      params: {
        patientId: id,
      },
    };

    const res: any = {
      status: function () {
        return this;
      },
      json: function () {},
    };

    patientController
      .readAmountPayable(req, res, () => {})
      .then((readAmountPayable) => {
        expect(readAmountPayable).toContain(id);
      });
  });

  it("Patient record is fetch", async () => {
    const req: any = {
      body: {},
    };

    const res: any = {
      status: function () {
        return this;
      },
      json: function () {},
    };

    patientController
      .readAll(req, res, () => {})
      .then((readAll) => {
        expect(readAll).toContain('Dog');
      });
  });

  it("Patient record is update", async () => {
    const req: any = {
      params: {
        patientId: id,
      },
      body: {
        petName: "Shaka",
        petType: "Dog",
        ownerName: "Rafique",
        ownerAddress: "New Address",
        ownerPhone: "1234567890",
      },
    };

    const res: any = {
      status: function () {
        return this;
      },
      json: function () {},
    };

    patientController
      .updatePatient(req, res, () => {})
      .then((updatePatient) => {
        expect(updatePatient).toContain(id);
      });
  });

  it("patient record is delete", async () => {
    const req: any = {
      params: {
        patientId: id,
      },
    };

    const res: any = {
      status: function () {
        return this;
      },
      json: function () {},
    };

    patientController
      .readAmountPayable(req, res, () => {})
      .then((readAmountPayable) => {
        expect(readAmountPayable).toContain(id);
      });
  });
});
