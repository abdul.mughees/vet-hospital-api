jest.useFakeTimers()
import appointmentController from "./appointmentController";
jest.useFakeTimers()
import mongoose from "mongoose";

const id = new mongoose.Types.ObjectId();

describe(" patient Appointment", () => {
  it("Appointment  is create", async () => {
    const req: any = {
      body: {
        _id: id,
        startTime: "2022/01/01",
        endTime: "2022/01/30",
        description: "test",
        paymentCurrency: "usd",
        amount: "100",
        patient: "Rafique",
      },
    };

    const res: any = {
      status: function () {
        return this;
      },
      json: function () {},
    };

    appointmentController
      .createAppointment(req, res, () => {})
      .then((savedAppointment) => {
        expect(savedAppointment).toContain(id);
      });
  });

  it("appointment is Read", async () => {
    const req: any = {
      params: {
        patientId: id,
      },
    };

    const res: any = {
      status: function () {
        return this;
      },
      json: function () {},
    };

    appointmentController
      .readAppointment(req, res, () => {})
      .then((readAppointment) => {
        expect(readAppointment).toContain(id);
      });
  });

  it("By Patient appointment is Read", async () => {
    const req: any = {
      body: {
        patient: id,
      },
    };

    const res: any = {
      status: function () {
        return this;
      },
      json: function () {},
    };

    appointmentController
      .readAppointmentsByPatient(req, res, () => {})
      .then((readAppointment) => {
        expect(readAppointment).toContain(id);
      });
  });

  it("By Date appointment is Read", async () => {
    const req: any = {
      body: {
        date: "2022/01/15",
      },
    };

    const res: any = {
      status: function () {
        return this;
      },
      json: function () {},
    };

    appointmentController
      .readAppointmentsByDate(req, res, () => {})
      .then((readAppointment) => {
        expect(readAppointment).toContain('2022/01/15');
      });
  });

  it("By unpaid appointment is Read", async () => {
    const req: any = {
      body: {},
    };

    const res: any = {
      status: function () {
        return this;
      },
      json: function () {},
    };

    appointmentController
      .readUnpaidAppointments(req, res, () => {})
      .then((readAppointment) => {
        expect(readAppointment).toContain('amount');
      });
  });

  it("all patient appointment is Read", async () => {
    const req: any = {
      body: {},
    };

    const res: any = {
      status: function () {
        return this;
      },
      json: function () {},
    };

    appointmentController
      .readAll(req, res, () => {})
      .then((readAppointment) => {
        expect(readAppointment).toContain(id);
      });
  });

  it("appointment is update", async () => {
    const req: any = {
      params: {
        appointmentId: id,
      },
      body: {
        _id: id,
        startTime: "2022/01/01",
        endTime: "2022/01/30",
        description: "test",
        paymentCurrency: "usd",
        amount: "100",
        patient: "Rafique",
      },
    };

    const res: any = {
      status: function () {
        return this;
      },
      json: function () {},
    };

    appointmentController
      .updateAppointment(req, res, () => {})
      .then((readAppointment) => {
        expect(readAppointment).toContain(id);
      });
  });

  it("appointment is delete", async () => {
    const req: any = {
      params: {
        appointmentId: id,
      },
    };

    const res: any = {
      status: function () {
        return this;
      },
      json: function () {},
    };

    appointmentController
      .deleteAppointment(req, res, () => {})
      .then((readAppointment) => {
        expect(readAppointment).toContain(id);
      });
  });
});
