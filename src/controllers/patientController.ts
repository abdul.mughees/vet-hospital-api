import { Request, Response, NextFunction } from "express";
import mongoose from "mongoose";
import Patient from "../models/Patient";
import Appointment from "../models/Appointment";


const createPatient = (req: Request, res: Response, next: NextFunction) => {
    const { petName, petType, ownerName, ownerAddress, ownerPhone } = req.body;

    const patient = new Patient({
        _id: new mongoose.Types.ObjectId(),
        petName: petName,
        petType: petType,
        ownerName: ownerName,
        ownerAddress: ownerAddress,
        ownerPhone: ownerPhone
    });

    return patient
        .save()
        .then(patient => res.status(201).json({ patient }))
        .catch(error => res.status(500).json({ message: error }));
}

const readPatient = (req: Request, res: Response, next: NextFunction) => {
    const patientId = req.params.patientId;

    return Patient.findById(patientId)
        .then(patient => patient ? res.status(200).json({ patient }) : res.status(404).json({ message: "Not Found" }))
        .catch(error => res.status(500).json({ message: error }));
}

const readAmountPayable = (req: Request, res: Response, next: NextFunction) => {
    const patientId = req.params.patientId;
    let ammountPayable = 0;

    return  Appointment
        .find({ $and: [{ paymentCurrency: new RegExp("unpaid", "i") }, { id: patientId }] })
        .then(appointments => {
            appointments.forEach(function (appointment) {
                ammountPayable += Number(appointment.amount);
            });
            res.status(200).json({ ammountPayable: ammountPayable })
        })
        .catch(error => res.status(500).json({ message: error }));
}

const readAll = (req: Request, res: Response, next: NextFunction) => {
    return Patient.find()
        .then((patients) => res.status(200).json({ patients }))
        .catch(error => res.status(500).json({ message: error }));
}

const updatePatient = (req: Request, res: Response, next: NextFunction) => {
    const patientId = req.params.patientId;

    return Patient.findById(patientId)
        .then((patient) => {
            if (patient) {
                patient.set(req.body)

                return patient
                    .save()
                    .then(patient => res.status(201).json({ patient }))
                    .catch(error => res.status(500).json({ error }));

            } else {
                res.status(404).json({ message: "Not Found" })
            }
        })
        .catch(error => res.status(500).json({ message: error }));
}

const deletePatient = (req: Request, res: Response, next: NextFunction) => {
    const patientId = req.params.patientId;

    return Patient
        .findByIdAndDelete(patientId)
        .then(patient => res.status(201).json({ message: "Deleted" }))
        .catch(error => res.status(500).json({ message: error }));
}

export default { createPatient, readPatient, readAmountPayable, readAll, updatePatient, deletePatient }