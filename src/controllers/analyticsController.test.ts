jest.useFakeTimers()
import analyticsController from "./analyticsController";
describe("delete patient record ", () => {
  describe("Read Pet Report", () => {
    it("pet report is read", async () => {
      const req:any = {
        body: {},
      };

      const res : any = {
        status: function () {
          return this;
        },
        json: function () {},
      };

      analyticsController
        .readPetReport(req, res, () => {})
        .then((readPetReport) => {
          expect(readPetReport).toContain('Dog');
        });
    });
  });
});
