import { Request, Response, NextFunction } from "express";
import mongoose from "mongoose";
import Appointment from "../models/Appointment";
import Patient from "../models/Patient";


const createAppointment = (req: Request, res: Response, next: NextFunction) => {
    const { startTime, endTime, description, paymentCurrency, amount, patient } = req.body;

    const appointment = new Appointment({
        _id: new mongoose.Types.ObjectId(),
        startTime: startTime,
        endTime: endTime,
        description: description,
        paymentCurrency: paymentCurrency,
        amount: amount,
        patient: patient
    });

    return appointment
        .save()
        .then(appointment => res.status(201).json({ appointment }))
        .catch(error => res.status(500).json({ message: error }));
}

const readAppointment = (req: Request, res: Response, next: NextFunction) => {
    const appointmentId = req.params.appointmentId;

    return Appointment.findById(appointmentId)
        .populate('patient')
        .then(appointment => appointment ? res.status(200).json({ appointment }) : res.status(404).json({ message: "Not Found" }))
        .catch(error => res.status(500).json({ message: error }));
}

const readAppointmentsByPatient = (req: Request, res: Response, next: NextFunction) => {
    const { patient } = req.body;

    return Appointment
        .find({ patient: patient })
        .populate('patient')
        .then(appointment => appointment ? res.status(200).json({ appointment }) : res.status(404).json({ message: "Not Found" }))
        .catch(error => res.status(500).json({ message: error }));
}

const readAppointmentsByDate = (req: Request, res: Response, next: NextFunction) => {
    const { date } = req.body;

    return Appointment
        .find({ "startTime": new RegExp(date, "i") })
        .populate('patient')
        .then(appointments => appointments ? res.status(200).json({ appointments }) : res.status(404).json({ message: "Not Found" }))
        .catch(error => res.status(500).json({ message: error }));
}

const readUnpaidAppointments = (req: Request, res: Response, next: NextFunction) => {

    return Appointment
        .find({ "paymentCurrency": new RegExp("unpaid", "i") })
        .populate('patient')
        .then(appointments => appointments ? res.status(200).json({ appointments }) : res.status(404).json({ message: "Not Found" }))
        .catch(error => res.status(500).json({ message: error }));
}


const readAll = (req: Request, res: Response, next: NextFunction) => {
    return Appointment.find()
        .populate('patient')
        .then((appointments) => res.status(200).json({ appointments }))
        .catch(error => res.status(500).json({ message: error }));
}

const updateAppointment = (req: Request, res: Response, next: NextFunction) => {
    const appointmentId = req.params.appointmentId;

    return Appointment.findById(appointmentId)
        .then((appointment) => {
            if (appointment) {
                appointment.set(req.body)

                return appointment
                    .save()
                    .then(appointment => res.status(201).json({ appointment }))
                    .catch(error => res.status(500).json({ error }));

            } else {
                res.status(404).json({ message: "Not Found" })
            }
        })
        .catch(error => res.status(500).json({ message: error }));
}

const deleteAppointment = (req: Request, res: Response, next: NextFunction) => {
    const appointmentId = req.params.appointmentId;

    return Appointment
        .findByIdAndDelete(appointmentId)
        .then(appointment => res.status(201).json({ message: "Deleted" }))
        .catch(error => res.status(500).json({ message: error }));
}

export default { createAppointment, readAppointment, readAppointmentsByPatient, readUnpaidAppointments, readAppointmentsByDate, readAll, updateAppointment, deleteAppointment }