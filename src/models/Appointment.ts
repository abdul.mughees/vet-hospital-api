import mongoose, { Document, Schema } from "mongoose";

export interface IAppointment {
    startTime: string;
    endTime: string;
    description: string;
    paymentCurrency: string;
    amount: string;
    patient: string;
    type: string;
}

export interface IAppointmentModel extends IAppointment, Document {}

const AppointmentSchema: Schema = new Schema(
    {
        patient: {type: String, required: true, ref: 'Patient'},
        startTime: { type: String, required: true },
        endTime: { type: String, required: true },
        description: { type: String, required: true },
        paymentCurrency: { type: String, required: true },
        amount: { type: String, required: true }
    },
    {
        timestamps: true,
        versionKey: false
    }
)

export default mongoose.model<IAppointmentModel>('Appointment', AppointmentSchema);