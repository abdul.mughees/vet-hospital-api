import mongoose, { Document, Schema } from "mongoose";

export interface IPatient {
    petName: string;
    petType: string;
    ownerName: string;
    ownerAddress: string;
    ownerPhone: string;
    appointsments: string;
}

export interface IPatientModel extends IPatient, Document {}

const PatientSchema: Schema = new Schema(
    {
        petName: { type: String, required: true },
        petType: { type: String, required: true },
        ownerName: { type: String, required: true },
        ownerAddress: { type: String, required: true },
        ownerPhone: { type: String, required: true },
        appointsments: {type: String, ref: 'Appointment'}
    },
    {
        versionKey: false
    }
)

export default mongoose.model<IPatientModel>('Patient', PatientSchema);