import express from "express";
import http from "http";
import mongoose from "mongoose";
import { config } from "./config/config";
import patientRoutes from "./routes/Patient";
import appointmentRoutes from "./routes/Appointment";
import analyticsRoutes from "./routes/Analytics";

const router = express();


/** Only starts the server if Mongo Connects */

const StartServer = () => {
    router.use((req, res, next) => {
      /** Log the request */
      console.log(
        `Incoming -> Method: [${req.method}] - Url: [${req.url}] - IP: [${req.socket.remoteAddress}]`
      );
  
      res.on("finish", () => {
        /** Log the response */
        console.log(
          `Incoming -> Method: [${req.method}] - Url: [${req.url}] - IP: [${req.socket.remoteAddress}] - Status: [${res.statusCode}]`
        );
      });
  
      next();
    });
  
    router.use(express.urlencoded({ extended: true }));
    router.use(express.json());
  
    /** Routes */
    router.use("/patients", patientRoutes);
    router.use("/appointments", appointmentRoutes);
    router.use("/analytics", analyticsRoutes);
  
    /** Healthcheck */
    router.get("/ping", (req, res, next) =>
      res.status(200).json({ message: "pong" })
    );
  
    /** Error Handling */
    router.use((req, res, next) => {
      const error = "not found";
      console.log(error);
  
      return res.status(404).json({ message: error });
    });
  
    http
      .createServer(router)
      .listen(config.server.port, () =>
        console.log(`Server is running on port ${config.server.port}`)
      );
  };
  

/* Connect to MongoDB */
const app = async () => {
  try {
    await mongoose.connect(config.mongo.url, {
      retryWrites: true,
      w: "majority",
    });
    StartServer();
  } catch (error) {
    console.log("Unable to connect: ", error);
  }
};

export default app();